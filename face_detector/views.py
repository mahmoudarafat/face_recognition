from django.shortcuts import render

from django.db import connection
from django.conf import settings
from imutils import paths
import face_recognition
import argparse
import pickle
import numpy as np
import urllib
import json
import os
import cv2
from django.views.decorators.csrf import csrf_exempt


from django.http import JsonResponse

# start off with defining a function to detect the URL requested which has the image for facial recognition
@csrf_exempt

def requested_url(request):
    #default value set to be false

    default = {"status": False} #because no detection yet

    ## between GET or POST, we go with Post request and check for https

    if request.method == "POST":
        if request.FILES.get("image", None) is not None:
            encodings = upload_image(request)

            pickle_file = os.path.join(settings.BASE_DIR, 'encodings.pickle')

            data = pickle.loads(open(pickle_file, "rb").read())

            names = []
            predicted_user_ids = [];
            detected = 0
            # loop over the facial embeddings
            for encoding in encodings:

                detected += 1
                # attempt to match each face in the input image to our known
                #    encodings
                matches = face_recognition.compare_faces(data["encodings"],
                                                     encoding)
                name = "Unknown"

                #    check to see if we have found a match
                if True in matches:
                    # find the indexes of all matched faces then initialize a
                    # dictionary to count the total number of times each face
                    # was matched
                    matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                    counts = {}

                    # loop over the matched indexes and maintain a count for
                    # each recognized face face
                    for i in matchedIdxs:
                        name = data["names"][i]
                        counts[name] = counts.get(name, 0) + 1

                    # determine the recognized face with the largest number of
                    # votes (note: in the event of an unlikely tie Python will
                    # select first entry in the dictionary)
                    name = max(counts, key=counts.get)
                    # update the list of names
                    names.append(name)

                    u_id = int(filter(str.isdigit, name))
                    predicted_user_ids.append(u_id)

            lat = request.POST.get('lat', None)
            lng = request.POST.get('lng', None)
            user_id = request.POST.get('user_id', None)
            nearbyUsers = findUsersNearby(lat, lng, user_id)
            #users = nearbyUsers[0]
            
            matched = []
            #for obj in users:
            #    matched.append(obj['user_id'])

            common_matched = set(nearbyUsers).intersection(predicted_user_ids)
            common_matched = list(common_matched)


            #### FIND USERS ####
            if common_matched:
                common = findUsers(common_matched)
            else:
                common = []
            if predicted_user_ids:
                predicted = findUsers(predicted_user_ids)
            else:
                predicted = []
            if nearbyUsers:
                nearby = findUsers(nearbyUsers)
            else:
                nearby = []
                
            default.update({
                "status": True,
                "matched": common,
                "predicted_users": predicted,
                "near_users": nearby,
                #"#detected_faces": detected,
                #"predicted_faces":names,
                })

            return JsonResponse(default)
        else: # URL is provided by the user
            return JsonResponse(default)
    else:
        return JsonResponse(default)

def createTrainer(request):
    # initialize the list of known encodings and known names
    knownEncodings = []
    knownNames = []
        
    # grab the paths to the input images in our dataset
    # print("[INFO] quantifying faces...")
    pathDB = os.path.join(settings.BASE_DIR, 'dataset')
    imagePaths = list(paths.list_images(pathDB))
    PickleFile = "encodings.pickle"
    
    if os.path.isfile(PickleFile):
        #print 'found encoding file'
        data = pickle.loads(open(PickleFile, "rb").read())
        #print "==================================="
        knownEncodings = data['encodings']
        knownNames = data['names']

        # loop over the image paths
        for (i, imagePath) in enumerate(imagePaths):
            # extract the person name from the image path
            print("[INFO] processing image {}/{}".format(i + 1, len(imagePaths)))
            name = imagePath.split(os.path.sep)[-2]
            if not name in knownNames:
                # load the input image and convert it from RGB (OpenCV ordering)
                # to dlib ordering (RGB)
                image = cv2.imread(imagePath)
                rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                # detect the (x, y)-coordinates of the bounding boxes
                # corresponding to each face in the input image
                boxes = face_recognition.face_locations(rgb,
                        model="knn")

                # compute the facial embedding for the face
                encodings = face_recognition.face_encodings(rgb, boxes)

                # loop over the encodings
                for encoding in encodings:
                        # add each encoding + name to our set of known names and
                        # encodings
                        knownEncodings.append(encoding)
                        knownNames.append(name)
        
    else:        

        # loop over the image paths
        for (i, imagePath) in enumerate(imagePaths):
                # extract the person name from the image path
                print("[INFO] processing image {}/{}".format(i + 1,
                        len(imagePaths)))
                name = imagePath.split(os.path.sep)[-2]

                # load the input image and convert it from RGB (OpenCV ordering)
                # to dlib ordering (RGB)
                image = cv2.imread(imagePath)
                rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                # detect the (x, y)-coordinates of the bounding boxes
                # corresponding to each face in the input image
                boxes = face_recognition.face_locations(rgb,
                        model="knn")

                # compute the facial embedding for the face
                encodings = face_recognition.face_encodings(rgb, boxes)

                # loop over the encodings
                for encoding in encodings:
                        # add each encoding + name to our set of known names and
                        # encodings
                        knownEncodings.append(encoding)
                        knownNames.append(name)

    # dump the facial encodings + names to disk
    #print("[INFO] serializing encodings...")
    data = {"encodings": knownEncodings, "names": knownNames}
    f = open(PickleFile, "wb")
    f.write(pickle.dumps(data))
    f.close()

    return JsonResponse({"status": True})


def allowed_file(filename):
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def upload_image(request):
    file = request.FILES.get("image", None)
    try:
        return faces_encoding(file)
    except:
        return [];

def faces_encoding(file_stream):
    img = face_recognition.load_image_file(file_stream)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # Get face encodings for any faces in the uploaded image
    unknown_face_encodings = face_recognition.face_encodings(img)

    return unknown_face_encodings


def findUsersNearby(lat, lng, user_id):
    # if request.method == "GET":
    unit = float(6378.10)  # for kilometers
    radius = float(0.010)
    data = []
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT users.id, users.name, ( " + str(unit) + " * acos (cos ( radians(" + str(
                lat) + ") ) * cos( radians( markers.lat ) ) * cos( radians(" + str(
                lng) + ") -  radians(markers.lon ) ) + sin ( radians(" + str(
                lat) + ") ) * sin( radians( markers.lat ) )) ) AS distance FROM markers JOIN users ON users.id=markers.user_id"
                       " HAVING ( distance < " + str(radius) + "  and users.id != " + str(
                user_id) + " ) ORDER BY distance LIMIT 0 , 20;")
        row = cursor.fetchall()
        users = []
        ids = []
        for user in row:
            distance = user[2]
            distance = str(distance)
            distance = distance[:9]
            distance = float(distance) * 1000
            user_id = user[0]
            user_name = user[1]
            data.append({'user_id': user_id, 'user_name': user_name, 'distance': distance})
            users.append({"user_id":user_id, "name": user_name})

            ids.append(user_id)
    #return [data, users]
    return ids

def findUsers(user_ids):
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT users.id, users.name FROM users WHERE users.id in (" + ','.join(str(e) for e in user_ids) + ") LIMIT 0 , 20;")
        row = dictfetchall(cursor)
    return row

def dictfetchall(cursor): 
    "Returns all rows from a cursor as a dict" 
    desc = cursor.description 
    return [
            dict(zip([col[0] for col in desc], row)) 
            for row in cursor.fetchall() 
    ]

