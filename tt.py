import face_recognition

# Load the jpg files into numpy arrays
messi_image = face_recognition.load_image_file("dataset/messi/1.jpg")
salah_image = face_recognition.load_image_file("dataset/salah/11.jpg")
unknown_image = face_recognition.load_image_file("dataset/messi/16.jpg")

# Get the face encodings for each face in each image file
# Since there could be more than one face in each image, it returns a list of encodings.
# But since I know each image only has one face, I only care about the first encoding in each image, so I grab index 0.
try:
    messi_face_encoding = face_recognition.face_encodings(messi_image)[0]
    salah_face_encoding = face_recognition.face_encodings(salah_image)[0]
    unknown_face_encoding = face_recognition.face_encodings(unknown_image)[0]
except IndexError:
    print("I wasn't able to locate any faces in at least one of the images. Check the image files. Aborting...")
    quit()

known_faces = [
    messi_face_encoding,
    salah_face_encoding
]

# results is an array of True/False telling if the unknown face matched anyone in the known_faces array
results = face_recognition.compare_faces(known_faces, unknown_face_encoding)

print("This is Messi? {}".format(results[0]))
print("This is Salah? {}".format(results[1]))
print("we've never seen this before? {}".format(not True in results))
